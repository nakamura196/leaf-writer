import type { User } from '../../types';

export const state: User = {
  name: 'anonymous',
  uri: 'http://anonymous',
};
