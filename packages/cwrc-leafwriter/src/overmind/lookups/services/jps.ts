import axios from 'axios';
import type { AuthorityLookupResult } from '../../../dialogs/entityLookups/types';
import { log } from './../../../utilities';
import { type AuthorityLookupParams } from './type';

type NamedEntityType = 'person' | 'place';

interface SearchResponse {
  results: {
    bindings: {
      s: { value: string };
      label: { value: string };
      description: { value: string };
    }[];
  };
}

const BASE_URL = 'https://jpsearch.go.jp/rdf/sparql/';
const MAX_HITS = 10;
const TIMEOUT = 3000;

const axiosInstance = axios.create({ baseURL: BASE_URL, timeout: TIMEOUT });

// Function to construct SPARQL query
const buildSparqlQuery = (query: string, type: NamedEntityType): string => {
  const typePrefix = `https://jpsearch.go.jp/term/type/`;

  const jpsType = type === 'person' ? `type:Person` : `type:Place`;

  return `
      PREFIX type: <${typePrefix}>
      SELECT DISTINCT ?s ?label ?description WHERE {
        ?s a ${jpsType}; rdfs:label ?label .
        OPTIONAL { ?s schema:description ?description . }
        FILTER(bif:contains(?label, '"${query}"'))
      }
      LIMIT ${MAX_HITS}
    `;
};

export const find = async ({ query, type }: AuthorityLookupParams) => {
  if (type === 'person') return await callJps(query, 'person');
  if (type === 'place') return await callJps(query, 'place');

  log.warn(`JPS: Entity type ${type} invalid`);
};

const callJps = async (query: string, methodName: NamedEntityType) => {
  const sparqlQuery = buildSparqlQuery(query, methodName);

  const encodedQuery = encodeURIComponent(sparqlQuery);

  const url = `?output=json&query=${encodedQuery}`;

  const response = await axiosInstance.get<SearchResponse>(url).catch((error) => {
    return {
      status: 500,
      statusText: `The request exeeded the timeout (${TIMEOUT})`,
      data: undefined,
    };
  });

  if (response.status >= 400) {
    const errorMsg = `
          Something wrong with the call to Wikidata, possibly a problem with the network or the server.
          HTTP error: ${response.statusText}
        `;
    log.warn(errorMsg);
    return [];
  }

  const data = response.data;
  if (!data) return [];

  const results: AuthorityLookupResult[] = data.results.bindings.map(
    ({ s, label, description }) => {
      return {
        description: description?.value,
        id: s.value,
        name: label.value,
        repository: 'jps',
        query,
        type: methodName,
        uri: s.value,
      };
    },
  );

  return results;
};
