import CitationDialog from './CitationDialog';
import CorrectionDialog from './CorrectionDialog';
import DateDialog from './DateDialog';
import KeywordDialog from './KeywordDialog';
import LinkDialog from './LinkDialog';
import NoteDialog from './NoteDialog';
import OrganizationDialog from './OrganizationDialog';
import PersonDialog from './PersonDialog';
import PlaceDialog from './PlaceDialog';
import RsDialog from './RsDialog';
import TitleDialog from './TitleDialog';

export { CitationDialog };
export { DateDialog };
export { KeywordDialog };
export { LinkDialog };
export { NoteDialog };
export { OrganizationDialog };
export { PersonDialog };
export { PlaceDialog };
export { RsDialog };
export { TitleDialog };

export default {
  citation: CitationDialog,
  correction: CorrectionDialog,
  date: DateDialog,
  keyword: KeywordDialog,
  link: LinkDialog,
  note: NoteDialog,
  organization: OrganizationDialog,
  person: PersonDialog,
  place: PlaceDialog,
  rs: RsDialog,
  title: TitleDialog,
};
