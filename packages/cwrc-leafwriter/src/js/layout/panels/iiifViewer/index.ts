import $ from 'jquery';
import 'jquery-ui';
import Writer from '../../../Writer';
// @ts-ignore
import Mirador from 'mirador';

interface IiifViewerProps {
  attribute?: string;
  parentId: string;
  tag?: string;
  writer: Writer;
}

class IiifViewer {
  readonly writer: Writer;
  readonly id: string;
  readonly tagName: string;
  readonly attrName: string;

  // eslint-disable-next-line @typescript-eslint/no-explicit-any, @typescript-eslint/no-redundant-type-constituents
  miradorInstance: any | null;

  $pageBreaks: unknown;
  currentIndex = -1;
  ignoreScroll = false;

  constructor({ attribute, parentId, tag, writer }: IiifViewerProps) {
    this.writer = writer;
    this.id = `${parentId}_iiifViewer`;
    this.tagName = tag ?? 'pb'; // page break element name
    this.attrName = attribute ?? 'facs'; // attribute that stores the image URL

    $(`#${parentId}`).append(`
      <div id="${this.id}" style="position: absolute; top: 0; bottom: 0; left: 0; right: 0"></div>
    `);

    this.writer.event('loadingDocument').subscribe(() => this.reset());

    this.writer.event('documentLoaded').subscribe((success: boolean, body: HTMLElement) => {
      console.log('documentLoaded', success, body);
      if (!success) return;

      this.processDocument(body);
    });

    this.writer.event('writerInitialized').subscribe(() => {
      if (!this.writer.editor) return;
    });
  }

  private processDocument(doc: HTMLElement) {
    // (doc).find

    const $facsimile = $(doc).find(`*[_tag="facsimile"]`);

    const manifestUri = $facsimile.attr('sameas');

    const config = {
      id: this.id,
      windows: [
        {
          loadedManifest: manifestUri,
        },
      ],
      window: {
        sideBarOpen: false,
      },
    };

    // eslint-disable-next-line @typescript-eslint/no-unsafe-assignment, @typescript-eslint/no-unsafe-call, @typescript-eslint/no-unsafe-member-access
    this.miradorInstance = Mirador.viewer(config);
  }

  reset() {
    this.$pageBreaks = null;
    this.currentIndex = -1;
  }
}

export default IiifViewer;
